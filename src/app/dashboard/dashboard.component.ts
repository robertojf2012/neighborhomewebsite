import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../apiRest/api-rest.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user: any;

  //data: any;

  constructor(public apiRest: ApiRestService, private router: Router) { }

  ngOnInit() {
    this.checkSession();
  }
  
  checkSession(){
  	this.apiRest.checkSession().toPromise().then(resp =>{

      console.log("LOGIN!");
      
    }).catch( err => {
        console.log(err);

        if(err.status == 401){
          alert("Inicia sesión de nuevo");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
        if(err.status == 500){
          alert("Error de servidor");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
      });
  }
  
}
