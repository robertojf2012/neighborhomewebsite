import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { ApiRestService } from './apiRest/api-rest.service';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { CasasComponent } from './casas/casas.component';
import { PagosComponent } from './pagos/pagos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//ANGULAR MATERIAL
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { RegisterDialog } from './usuarios/usuarios.component';
import { RegisterHomeDialog } from './casas/casas.component';
import { RegisterCuotaDialog } from './pagos/pagos.component';
import { RegisterPagoDialog } from './pagos/pagos.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { RegisterNoticiaDialog } from './noticias/noticias.component';



//ANGULAR MATERIAL

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'casas', component: CasasComponent },
  { path: 'pagos', component: PagosComponent },
  { path: 'noticias', component: NoticiasComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    UsuariosComponent,
    CasasComponent,
    PagosComponent,
    RegisterDialog,
    RegisterHomeDialog,
    RegisterCuotaDialog,
    RegisterPagoDialog,
    RegisterNoticiaDialog,
    NoticiasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTabsModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [ 
    RegisterDialog,
    RegisterHomeDialog,
    RegisterCuotaDialog,
    RegisterPagoDialog,
    RegisterNoticiaDialog
  ],
  providers: [ApiRestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
