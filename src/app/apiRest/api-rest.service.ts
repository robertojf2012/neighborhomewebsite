import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiRestService {

	//api: string = 'http://localhost:3678/api/';
    api: string = 'https://neighborhomeapi.herokuapp.com/api/';

  //variable for showing or hidding top bar features
  public userLogged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  setLogin(logged: boolean){
    this.userLogged.next(logged);
  }

  logIn(email:string, password:string){
	  let data = {
		  	"email":email,
		    "password":password,
	  	};
	 	
    return this.http.post(this.api + 'login',data,{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  logOut(){
    return this.http.get(this.api + 'logout',{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  checkSession(){
    return this.http.get(this.api + 'session',{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  getFrac(idFrac: string){
    return this.http.get(this.api + 'frac/'+idFrac,{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  getUsers(idFrac: string){
    return this.http.get(this.api + 'users/'+idFrac,{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  postUser(idfrac:string,noCasa:number,email:string,password:string,nombre:string,paterno:string,materno:string,telefono:number,admin:boolean,pagos:string[]){
    let data = {
        "idFrac": idfrac,
        "intNoCasa": noCasa,
        "strEmail": email,
        "strPassword": password,
        "strNombre": nombre,
        "strApat": paterno,
        "strAmat": materno,
        "intTelefono": telefono,
        "boolAdmin": admin,
        "pagos": pagos
      };
     
    return this.http.post(this.api + 'newUser',data,{
      headers: {
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      },
      withCredentials: true
    });
  }

  setCasa(idFrac:string, noCasa:number, modelo:string, calle:string){
    let data = {
      "intNoCasa": noCasa,
      "strModelo": modelo,
      "strCalle": calle
    };

    return this.http.put(this.api + 'setCasa/'+idFrac,data,{
      headers:{
        'content-Type'  : 'application/json',
        'Accept'        : 'application/json'
      }
    });
  }

}
