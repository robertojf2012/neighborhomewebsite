import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { ApiRestService } from '../apiRest/api-rest.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  constructor(public dialog: MatDialog,public apiRest: ApiRestService,private router: Router) { }

  ngOnInit() {
    this.checkSession();
  }

  openDialog(){
    const dialogRef = this.dialog.open(RegisterNoticiaDialog, {
      width: '445px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  checkSession(){
    this.apiRest.checkSession().toPromise().then(resp =>{

      console.log("LOGIN!");
      
    }).catch( err => {
        console.log(err);

        if(err.status == 401){
          alert("Inicia sesión de nuevo");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
        if(err.status == 500){
          alert("Error de servidor");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
      });
  }


}


//////////////////// DIALOG CLASS ////////////////////
@Component({
  selector: 'registerNoticiaDialog',
  templateUrl: 'dialog.html',
})
export class RegisterNoticiaDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<RegisterNoticiaDialog>){}

  ngOnInit(){
  }
  onNoClick(): void {
    this.dialogRef.close();
  }


}
//////////////////// DIALOG CLASS ////////////////////

