import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../apiRest/api-rest.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	email: string = '';
	password: string = '';

  constructor(public apiRest: ApiRestService, private router: Router) { }

  ngOnInit() { }

  logIn(){

  	if(this.validateEmail(this.email) && this.validatePassword(this.password)){
  		
      this.apiRest.logIn(this.email,this.password).toPromise().then( resp => {
        
        localStorage.setItem("userData", JSON.stringify(resp));
        this.apiRest.setLogin(true);
                
        this.router.navigate(['dashboard']);

      }).catch( err => {
        console.log(err);
        if(err.status == 404){
          alert("Usuario o contrasena incorrecto");
        }
        if(err.status == 500){
          alert("Error de servidor");
        }
      });
  	}else{
  		console.log("DATA INCORRECT");
  	}

  }

	validateEmail(email: string) {
  	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
	}
	validatePassword(password: string){
		if(password!=""){
			return true;
		}else{
			return false;
		}
	}

}
