import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../apiRest/api-rest.service';
import { Router } from "@angular/router";
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {

	fracData: any;
	cuotas: any[];

  constructor(public apiRest: ApiRestService, private router: Router,public dialog: MatDialog) { }

  ngOnInit() {
  	this.checkSession();
  }

  checkSession(){
  	this.apiRest.checkSession().toPromise().then(resp =>{

      this.getCuotas();
      
    }).catch( err => {
        console.log(err);
        if(err.status == 401){
          alert("Inicia sesión de nuevo");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
        if(err.status == 500){
          alert("Error de servidor");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
      });
  }

  openDialog(){
    const dialogRef = this.dialog.open(RegisterCuotaDialog, {
      width: '445px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialog2(){
    const dialogRef = this.dialog.open(RegisterPagoDialog, {
      width: '445px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getCuotas(){
  	this.fracData = JSON.parse(localStorage.getItem("fracData"));
    this.cuotas = this.fracData.cuotas;
  }

}


//////////////////// DIALOG CLASS ////////////////////
@Component({
  selector: 'registerCuotaDialog',
  templateUrl: 'dialog.html',
})
export class RegisterCuotaDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<RegisterCuotaDialog>,public apiRest: ApiRestService){}

  ngOnInit(){
    
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
//////////////////// DIALOG CLASS ////////////////////


//////////////////// DIALOG CLASS ////////////////////
@Component({
  selector: 'registerPagoDialog',
  templateUrl: 'dialog2.html',
})
export class RegisterPagoDialog implements OnInit {

  fracData: any;
  casas: any[];

  constructor(public dialogRef: MatDialogRef<RegisterPagoDialog>,public apiRest: ApiRestService){}

  ngOnInit(){
    this.getCasas();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  getCasas(){
    this.fracData = JSON.parse(localStorage.getItem("fracData"));
    this.casas = this.fracData.casas;
  }

  pagar(){
    alert("Pago registrado exitosamente");
  }


}
//////////////////// DIALOG CLASS ////////////////////
