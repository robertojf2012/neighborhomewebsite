import { Component } from '@angular/core';
import { ApiRestService } from './apiRest/api-rest.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	user: any;
  frac: any;
	userName = '';
	userLogged: boolean = false;
  
  isAdmin: boolean = false;

  title = 'NeighborHome';
  description = '';
  
  constructor(public apiRest: ApiRestService, private router: Router) {}

  ngOnInit() {

    this.apiRest.checkSession().toPromise().then(resp =>{

      console.log("LOGIN! (from main component)");
      this.apiRest.setLogin(true);
      
    }).catch( err => {
        console.log("NO LOGIN "+err.status);
      });

    this.apiRest.userLogged.subscribe( value => {
      this.userLogged = value;
      this.getUserData();
    });

  }

  getUserData(){

    if(this.userLogged == true){
      //getting the data
      this.user = JSON.parse(localStorage.getItem("userData"));
      
      //console.log(this.user); //showing data
      
      if(this.user.boolAdmin == true){
        this.isAdmin = true;
      }else{
        this.isAdmin = false;
      }
      this.userName = this.user.strNombre;
      this.getFracData(this.user.idFrac);
    }

  }

  getFracData(id: string){

    this.apiRest.getFrac(id).toPromise().then( resp => {
      
      this.frac = resp;
      this.title = this.frac.strNombre;
      this.description = '( '+this.frac.strDescripcion+' )';
      //console.log(this.frac);
      localStorage.setItem("fracData", JSON.stringify(resp));

      }).catch( err => {
        console.log(err);
        if(err.status == 404){
          alert("No se encontro el fraccionamiento");
        }
        if(err.status == 500){
          alert("Error de servidor");
        }
      });
  }

  logOut(){
  	this.apiRest.logOut().toPromise().then(resp =>{
      
      localStorage.removeItem("userData");
      this.apiRest.setLogin(false);
      this.title = 'NeighborHome';
      this.description = '';
      this.router.navigate(['']);
    }).catch( err => {
        console.log(err.statusText);
        this.router.navigate(['']);
      });
  }

}
