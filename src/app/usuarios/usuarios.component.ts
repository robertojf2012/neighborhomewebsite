import { Component, OnInit, Inject } from '@angular/core';
import { ApiRestService } from '../apiRest/api-rest.service';
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
	
	users: any;
	userData: any;
  admin: boolean = false;
  
  constructor(public apiRest: ApiRestService, private router: Router, public dialog: MatDialog) { }

  ngOnInit() {
  	this.checkSession();
  }

  checkSession(){
  	this.apiRest.checkSession().toPromise().then(resp =>{

      this.getUsers();
      
    }).catch( err => {
        console.log(err);
        if(err.status == 401){
          alert("Inicia sesión de nuevo");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
        if(err.status == 500){
          alert("Error de servidor");
          
          localStorage.removeItem("userData");
          this.apiRest.setLogin(false);
          this.router.navigate(['']);
        }
      });
  }

  getUsers(){
  	this.userData = JSON.parse(localStorage.getItem("userData"));
    this.admin = this.userData.boolAdmin;
  	this.apiRest.getUsers(this.userData.idFrac).toPromise().then(resp =>{

  		this.users = resp;
  		//console.log(this.users);
    }).catch( err => {
        console.log(err);
      });
  }

  openDialog(){
    const dialogRef = this.dialog.open(RegisterDialog, {
      width: '445px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}


//////////////////// DIALOG CLASS ////////////////////
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog.html',
})
export class RegisterDialog implements OnInit {

  nombre: string = '';
  aPat: string = '';
  aMat: string = '';
  noCasa: number = 0;
  email: string = '';
  telefono: number;
  password: string = '';
  password2: string = '';
  admin: boolean = false;
  pagos: string[] = [];

  fracData: any;
  casas: any[];

  constructor(public dialogRef: MatDialogRef<RegisterDialog>,public apiRest: ApiRestService){}

  ngOnInit() {
    this.getCasas();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  registerUser(){
    if(this.nombre!="" &&
       this.aPat!="" &&
       this.aMat!="" &&
       this.noCasa!=0 &&
       this.email!="" &&
       this.telefono &&
       this.password!="" &&
       this.password2!=""
      )
    {
      if(this.password == this.password2){
        
        this.apiRest.postUser(this.fracData._id,this.noCasa,this.email,this.password,this.nombre,this.aPat,this.aMat,this.telefono,this.admin,this.pagos).toPromise().then( resp => {
        
          alert("Usuario registrado exitosamente");
          this.onNoClick();

        }).catch( err => {
          console.log(err);
          if(err.status == 500){
            alert("Error de servidor");
          }
        });

      }else{
        alert("Las contrasenas no coinciden");
      }

    }else{
      alert("Campos vacios");      
    }
  }

  getCasas(){
    this.fracData = JSON.parse(localStorage.getItem("fracData"));
    this.casas = this.fracData.casas;
  }

}
//////////////////// DIALOG CLASS ////////////////////
