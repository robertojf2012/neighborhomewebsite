import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../apiRest/api-rest.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { Router } from "@angular/router";

@Component({
  selector: 'app-casas',
  templateUrl: './casas.component.html',
  styleUrls: ['./casas.component.css']
})
export class CasasComponent implements OnInit {

  fracData: any;
  casas: any[];

  constructor(public apiRest: ApiRestService,public dialog: MatDialog,private router: Router) { }

  ngOnInit() {
    this.checkSession();
  }

  checkSession(){
  this.apiRest.checkSession().toPromise().then(resp =>{

    this.getCasas();
    
  }).catch( err => {
      console.log(err);
      if(err.status == 401){
        alert("Inicia sesión de nuevo");
        
        localStorage.removeItem("userData");
        this.apiRest.setLogin(false);
        this.router.navigate(['']);
      }
      if(err.status == 500){
        alert("Error de servidor");
        
        localStorage.removeItem("userData");
        this.apiRest.setLogin(false);
        this.router.navigate(['']);
      }
    });
  }

  openDialog(){
    const dialogRef = this.dialog.open(RegisterHomeDialog, {
      width: '445px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getCasas(){
    this.fracData = JSON.parse(localStorage.getItem("fracData"));
    this.casas = this.fracData.casas;
  }

}


//////////////////// DIALOG CLASS ////////////////////
@Component({
  selector: 'registerHomeDialog',
  templateUrl: 'dialog.html',
})
export class RegisterHomeDialog implements OnInit {

	noCasa: number;
	modelo: string = "";
	calle: string = "";
	fracData: any;

  constructor(public dialogRef: MatDialogRef<RegisterHomeDialog>,public apiRest: ApiRestService){}

  ngOnInit(){
  	this.fracData = JSON.parse(localStorage.getItem("fracData"));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  registerCasa(){

  	if(this.noCasa && this.modelo!="" && this.calle!=""){
  		
  		this.apiRest.setCasa(this.fracData._id,this.noCasa,this.modelo,this.calle).toPromise().then( resp => {
        alert("Casa registrada exitosamente");
        this.onNoClick();
      }).catch( err => {
        console.log(err);
        if(err.status == 500){
          alert("Error de servidor");
        }
      });

  	}else{
  		alert("Campos erroneos o vacios");
  	}

  }

}
//////////////////// DIALOG CLASS ////////////////////
